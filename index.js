const Koa = require('koa')
const context = require('./context')
const logger = require('./logger')
const user = require('./user')

const app = new Koa()

app.use(async (ctx, next) => {
  await new Promise(context.namespace.bind(resolve => {
    resolve(next())
  }))
})

app.use(async (ctx, next) => {
  context.generateRequestId()

  await next()
})

app.use(async ctx => {
  await user.getUsers()
  ctx.body = `Hello world`
})

app.listen(3000)