const cls = require('cls-hooked')
const uuid = require('uuid/v4')

const ns = cls.createNamespace('test')

function setValue (key, value) {
  ns.set(key, value)
}

function getValue (key) {
  return ns.get(key)
}

function generateRequestId (id) {
  const requestId = id ? id : uuid()

  setValue('requestId', requestId)
}

function getRequestId () {
  return ns.get('requestId')
}

function setDBTime (time) {
  const currentDBTime = getDBTime()

  ns.set('dbTime', currentDBTime + time)
}

function getDBTime () {
  const dbTime = ns.get('dbTime')

  return dbTime ? dbTime : 0
}

module.exports = {
  namespace: ns,
  generateRequestId,
  getRequestId,
  setDBTime,
  getDBTime
}