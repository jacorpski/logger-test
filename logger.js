const pino = require('pino')()
const context = require('./context')

function info (msg) {
  const pinoContext = {
    requestId: context.getRequestId(),
    dbTime: context.getDBTime()
  }

  pino.info(pinoContext, msg)
}

module.exports = {
  info
}