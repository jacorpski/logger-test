const cls = require('cls-hooked')
const ns = cls.createNamespace('test')

function main () {
  ns.set('test', 'me')
  const test = ns.get('test')
  console.log('test', test)
}

ns.run(() => {
  main()
})