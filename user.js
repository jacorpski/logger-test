const logger = require('./logger')
const context = require('./context')

function randomNumber (min, max) {
  return Math.floor((Math.random() * max) + min)
}

async function sleep (time) {
  return new Promise(resolve => setTimeout(resolve, time))
}

async function getUsers () {
  const start = Date.now()
  logger.info('getting users')
  await sleep(randomNumber(100, 500))
  const end = Date.now() - start

  context.setDBTime(end)
  logger.info('users found')


  return [{ username: 'test' }, { username: 'me' }]
}

module.exports = {
  getUsers
}